"Scientific code needs to be free (as in free speech) and peer reviewed."

Download in a folder and run the R_file_supplemental.R file, which will source the needed files and compile the stan program.

You can also just source(R_file_supplemental.R) from the console, which will print tables and figures.