/*
 * -----------------------------------------------------
 * Thomas Roraas  <thomas.roraas@me.com>
 * -----------------------------------------------------
 * 3-level nested model for individual BV estimation
 * Predict Distribution
 * Relative robust for outliers, but always check data
 * visually, plot mean values and differences on all
 * levels, inspect and use common sense!
 *
 * -----------------------------------------------------
 * We log-transform the data before modelling to work with CV
 * -----------------------------------------------------
 * Please use good priors and initial start values
 * this speed up the model and prior knowledge is
 * of course of value and in the Bayesian philosophy!
 *
 * -----------------------------------------------------
 */
data {
  //total observations
  int<lower=0> Nobs;
  //total samples   
  int<lower=0> Nps; 
  //number of persons
  int<lower=0> Np; 
  //Vector with corresponding subject id for all obs
  int<lower=1,upper=Np> SubjectID[Nobs];
  //Vector with corresponding sample id for all obs
  int<lower=1,upper=Nps> SampleID[Nobs];
  //Corresponding SubjectID for each SampleID
  int<lower=1,upper=Np> SubjectLookUp[Nps];
  //measured value, typically this will be log-transformed
  //before running the model!
  vector[Nobs] y;
  //prior estimates for pop. mean and SDs
  real hyper_beta;
  real<lower=0> hyper_beta_sd;
  real<lower=0> hyper_sdg;
  real<lower=0> hyper_sdg_sd;
  real<lower=0> hyper_sda;
  real<lower=0> hyper_sda_sd;
  real<lower=0> hyper_mu_sdi;
  real<lower=0> hyper_mu_sdi_sd;
  real<lower=0> hyper_sigma_sdi;
  real<lower=0> hyper_sigma_sdi_sd;
  real<lower=2.1> hyper_nu_i;
  real<lower=0> hyper_nu_i_sd;
  real<lower=2.1> hyper_nu_a;
  real<lower=0> hyper_nu_a_sd;
}

parameters {
  // population mean ca be negative on log
  real beta; 
  // between person variation
  real<lower=0.0> sd_G; 
  // individual within-person variation
  real<lower=0.0> sd_i[Np];
   // measurement error
  real<lower=0.0> sd_A;
  // vector for person base value relative to pop. mean
  real beta_person[Np];
  // vector for sample base value relative to person mean
  real beta_sample[Nps]; 
  // mean within person variation
  real<lower=0.0> mu_sdi;
  // sd of within person variation
  real<lower=0.0> sigma_sdi; 
  //this is the parameters for the student-t dist
  //these are estimated from the data!
  real<lower=2.1> nu_sample;
  real<lower=2.1> nu_replicate;
}

model {
  vector[Nobs] yhat;

  //we have a prior based on knowledge
  beta ~ normal(hyper_beta, hyper_beta_sd);

  //student-t nu params
  nu_sample ~ normal(hyper_nu_i, hyper_nu_i_sd);
  nu_replicate ~ normal(hyper_nu_i, hyper_nu_i_sd);
  
  //priors for mean personal within-subject SD
  mu_sdi ~ normal(hyper_mu_sdi, hyper_mu_sdi_sd);
  //priors for variability between personal sdi
  sigma_sdi ~ normal(hyper_sigma_sdi, hyper_sigma_sdi_sd);
  //relates to above priors for within subject heterogeneity
  sd_i ~ normal(mu_sdi, sigma_sdi);
  
  //priors for hte other components of BV
  sd_G ~ normal(hyper_sdg, hyper_sdg_sd);
  sd_A ~ normal(hyper_sda, hyper_sda_sd);

  //-----------------------------------------------------
  // We use student-t for modelling data except 
  // for homeostatic setting points.
  // This makes the model more robust to outliers.
  //-----------------------------------------------------

  //likelihood homeostatic setting points
  beta_person ~ normal(beta, sd_G);

  //likelihood samples, vectorized! 
  beta_sample ~ student_t(nu_sample, beta_person[SubjectLookUp], sd_i[SubjectLookUp]);
  //for normal likelihood
  //beta_sample[i] ~ normal(beta_person[SubjectLookUp[i]], sd_i[SubjectLookUp[i]]);
  
  //so replicates get same base value from sample before analytical error added
  for (j in 1:Nobs){
    yhat[j] = beta_sample[SampleID[j]];
  }
  
  //likelihood for replicates;
  y ~ student_t(nu_replicate, yhat, sd_A);
  //for normal likelihood
  //y ~ normal(yhat, sd_A);
}

//used in predicting d(SD.p(i)) for group
generated quantities{
  real sd_pred;
  //sd_pred = normal_rng(mu_sdi, sigma_sdi);
  sd_pred = sqrt((normal_rng(mu_sdi, sigma_sdi)^2)*nu_sample/(nu_sample - 2));
  //for normal likelihood
  //sd_pred = student_t_rng(nu_sample, mu_sdi, sigma_sdi);
}
